#include "PositionSwipeMgr.h"

PositionSwipeMgr::PositionSwipeMgr()
{
	m_nPositionSwipeDataSize = 0;
	m_nMultiviewDataSize = 0;

	m_szPositionSwipeData = nullptr;
	m_szMultiviewData = nullptr;
	m_szMultiviewDataByChannel = nullptr;
}

PositionSwipeMgr::~PositionSwipeMgr()
{
	delete m_szPositionSwipeData;
}

int PositionSwipeMgr::AddPositionSwipeData(
	int nChannel, string strDscID,
	
	int nWidth, int nHeight, int nPositionSwipeLength,

	double dMoveX, double dMoveY,
	double dRotateX, double dRotateY,
	double dAngle, double dScale,
	int nMarginX,
	int nMarginY,
	int nMarginWidth,
	int nMarginHeight,

	int nX1, int nY1,
	int nX2, int nY2,
	int nX3, int nY3,
	int nX4, int nY4)
{
	StPositionSwipeData stPositionSwipeData;

	stPositionSwipeData.stImageInfo.nWidth = nWidth;
	stPositionSwipeData.stImageInfo.nHeight = nHeight;
	stPositionSwipeData.stImageInfo.nPositionSwipeLength = nPositionSwipeLength;

	stPositionSwipeData.stAdjustData.dMoveX = dMoveX;
	stPositionSwipeData.stAdjustData.dMoveY = dMoveY;
	stPositionSwipeData.stAdjustData.dRotateX = dRotateX;
	stPositionSwipeData.stAdjustData.dRotateY = dRotateY;
	stPositionSwipeData.stAdjustData.dAngle = dAngle;
	stPositionSwipeData.stAdjustData.dScale = dScale;

	stPositionSwipeData.stAdjustData.nMarginX = nMarginX;
	stPositionSwipeData.stAdjustData.nMarginY = nMarginY;
	stPositionSwipeData.stAdjustData.nMarginWidth = nMarginWidth;
	stPositionSwipeData.stAdjustData.nMarginHeight = nMarginHeight;

	stPositionSwipeData.stFourPoints.nX1 = nX1;
	stPositionSwipeData.stFourPoints.nY1 = nY1;
	stPositionSwipeData.stFourPoints.nX2 = nX2;
	stPositionSwipeData.stFourPoints.nY2 = nY2;
	stPositionSwipeData.stFourPoints.nX3 = nX3;
	stPositionSwipeData.stFourPoints.nY3 = nY3;
	stPositionSwipeData.stFourPoints.nX4 = nX4;
	stPositionSwipeData.stFourPoints.nY4 = nY4;

	int nRet = AddData(nChannel, strDscID, stPositionSwipeData);

	return nRet;		
}

int PositionSwipeMgr::ApplyPositionSwipeData()
{
	int nRet = 0;
	nRet = convertFourPointsUsingAdjustData();
	if (nRet != POSITION_SWIPE_OK)
		return nRet;

	nRet = generateHomographies();
	if (nRet != POSITION_SWIPE_OK)
		return nRet;

	return nRet;
}

int PositionSwipeMgr::convertFourPointsUsingAdjustData()
{
	int nRet = CheckValidOfList();
	if (nRet != LIST_MANAGER_OK) return nRet;

	for (int i = 0; i < GetListSize(); i++)
	{
		StPositionSwipeData stPositionSwipeData;
		GetDataByIndex(i, stPositionSwipeData);

		StImageInfo stImageInfo = stPositionSwipeData.stImageInfo;
		StAdjustData stAdjustData = stPositionSwipeData.stAdjustData;
		StFourPoints stFourPoints = stPositionSwipeData.stFourPoints;

		if (isFourPointsValid(i) == POSITION_SWIPE_ERROR_INVALID_FOURPOINTS)
			continue;

		Mat rotationMat;
		int nErr = getWarpMat(stPositionSwipeData.stAdjustData, rotationMat);
		if (nErr == POSITION_SWIPE_ERROR_INVALID_ADJUSTDATA)
			continue;

		convertSquare(stImageInfo, stAdjustData, rotationMat, stFourPoints.nX1, stFourPoints.nY1);
		convertSquare(stImageInfo, stAdjustData, rotationMat, stFourPoints.nX2, stFourPoints.nY2);
		convertSquare(stImageInfo, stAdjustData, rotationMat, stFourPoints.nX3, stFourPoints.nY3);
		convertSquare(stImageInfo, stAdjustData, rotationMat, stFourPoints.nX4, stFourPoints.nY4);

		setFourPointsByIndex(i, stFourPoints);
	}

	return nRet;
}

int PositionSwipeMgr::generateHomographies()
{
	int nRet = CheckValidOfList();
	if (nRet <= 0) return nRet;

	int nValidFourPoints = 0;
	nRet = getCountValidFourPointsInList(nValidFourPoints);
	if (nRet <= 0) return nRet;
	if (nValidFourPoints <= 1) return POSITION_SWIPE_ERROR_VALID_FOURPOINTS_SIZE_IS_LESS_THAN_TWO;

	int nIndex1 = GetListSize() - 1;
	int nIndex2 = 0;

	Mat mHomography;
	StFourPoints stFourPoints1;
	StFourPoints stFourPoints2;

	getFourPointsByIndex(nIndex1, stFourPoints1);
	getFourPointsByIndex(nIndex2, stFourPoints2);

	if (isFourPointsValid(nIndex2) == POSITION_SWIPE_OK)
	{
		while (isFourPointsValid(nIndex1) != POSITION_SWIPE_OK)
		{
			nIndex1--;
			if (nIndex1 < 0)
				nIndex1 = GetListSize() + nIndex1;
			getFourPointsByIndex(nIndex1, stFourPoints1);
		}
		m_HomographyMgr.CalcHomography(stFourPoints1, stFourPoints2, mHomography);
		addHomographyAtPairIndex(nIndex1, nIndex2, mHomography);
	}

	for (int i = 1; i < GetListSize(); i++)
	{
		nIndex1 = i - 1;
		nIndex2 = i;

		getFourPointsByIndex(nIndex1, stFourPoints1);
		getFourPointsByIndex(nIndex2, stFourPoints2);

		if (isFourPointsValid(nIndex2) == POSITION_SWIPE_OK)
		{
			while (isFourPointsValid(nIndex1) != POSITION_SWIPE_OK)
			{
				nIndex1--;
				if (nIndex1 < 0)
					nIndex1 = GetListSize() + nIndex1;
				getFourPointsByIndex(nIndex1, stFourPoints1);
			}
			m_HomographyMgr.CalcHomography(stFourPoints1, stFourPoints2, mHomography);
			addHomographyAtPairIndex(nIndex1, nIndex2, mHomography);
		}		
	}

	return POSITION_SWIPE_OK;
}

int PositionSwipeMgr::recalculatePositionSwipeData(int nIndex, int& nPosX, int& nPosY, int& nPositionLength)
{
	StPositionSwipeData stPositionSwipeData;
	GetDataByIndex(nIndex, stPositionSwipeData);

	Mat rotationMat;
	int nErr = getWarpMat(stPositionSwipeData.stAdjustData, rotationMat);
	if (nErr == POSITION_SWIPE_ERROR_INVALID_ADJUSTDATA)
		return POSITION_SWIPE_ERROR_INVALID_ADJUSTDATA;

	convertSquare(stPositionSwipeData.stImageInfo, stPositionSwipeData.stAdjustData, rotationMat, nPosX, nPosY);
	
	double dbMarginScale = 1.0;
	if (stPositionSwipeData.stAdjustData.nMarginWidth == 0 ||
		stPositionSwipeData.stImageInfo.nWidth == 0)
		dbMarginScale = 1.0;

	dbMarginScale = 
		static_cast<double>(stPositionSwipeData.stImageInfo.nWidth) / 
		static_cast<double>(stPositionSwipeData.stAdjustData.nMarginWidth);
	nPositionLength = static_cast<int>(nPositionLength * dbMarginScale);

	return POSITION_SWIPE_OK;
}

int PositionSwipeMgr::SetVecPositionAxis(int nChannel, int nZoomRatio, int nPosX, int nPosY)
{
	int nRet = CheckValidOfList();
	if (nRet <= 0) return nRet;

	int nValidFourPoints = 0;
	nRet = getCountValidFourPointsInList(nValidFourPoints);
	if (nRet <= 0) return nRet;
	if (nValidFourPoints <= 1) return POSITION_SWIPE_ERROR_VALID_FOURPOINTS_SIZE_IS_LESS_THAN_TWO;

	AscSortByKey();
	nRet = ShiftKeyToBegin(nChannel);
	if (nRet != LIST_MANAGER_OK) return nRet;

	StImageInfo stImageInfo;
	getImageInfoByIndex(0, stImageInfo);
	int nPositionSwipeLength = stImageInfo.nPositionSwipeLength;
	Point2d ptInputPoint = Point2d(nPosX, nPosY + nPositionSwipeLength/**fZoomRatio*/);

	StSwipePoint stSwipePoint;
	stSwipePoint.nZoomRatio = nZoomRatio;
	stSwipePoint.ptCenterPoint = ptInputPoint;
	stSwipePoint.ptViewPoint = Point2d(ptInputPoint.x, ptInputPoint.y - nPositionSwipeLength);

	setSwipePointByIndex(0, stSwipePoint);
	
	for (int i = 1; i < GetListSize(); i++)
	{
		int nIndex1 = i - 1;
		int nIndex2 = i;		

		Mat mHomography;
		if (isFourPointsValid(nIndex2) == POSITION_SWIPE_OK)
		{
			while (isFourPointsValid(nIndex1) != POSITION_SWIPE_OK)
			{
				nIndex1--;
				if (nIndex1 < 0)
					nIndex1 = GetListSize() + nIndex1;
			}

			getSwipePointByIndex(nIndex1, stSwipePoint);
			Point2d ptCenterPoint = stSwipePoint.ptCenterPoint;

			getHomographyAtPairIndex(nIndex1, nIndex2, mHomography);

			if (m_HomographyMgr.CalcPoint(mHomography, ptCenterPoint) != POSITION_SWIPE_OK) continue;

			StImageInfo stImageInfo;
			getImageInfoByIndex(nIndex2, stImageInfo);
			nPositionSwipeLength = stImageInfo.nPositionSwipeLength;

			stSwipePoint.nZoomRatio = nZoomRatio;
			stSwipePoint.ptCenterPoint = ptCenterPoint;
			stSwipePoint.ptViewPoint = Point2d(ptCenterPoint.x, ptCenterPoint.y - nPositionSwipeLength);

			setSwipePointByIndex(nIndex2, stSwipePoint);
		}
	}

	return nRet;
}

int PositionSwipeMgr::GetMovedAxis(int nChannel, int& nZoomRatio, int& nPosX, int& nPosY)
{
	for (int i = 0; i < GetListSize(); i++)
	{
		int nCurrentChannel = 0;
		GetKeyByIndex(i, nCurrentChannel);
		
		if (nCurrentChannel == nChannel)
		{
			StSwipePoint stSwipePoint;
			getSwipePointByIndex(i, stSwipePoint);

			Point2d ptViewPoint;

			nZoomRatio = stSwipePoint.nZoomRatio;
			nPosX = static_cast<int>(stSwipePoint.ptViewPoint.x);
			nPosY = static_cast<int>(stSwipePoint.ptViewPoint.y);

			return POSITION_SWIPE_OK;
		}
	}
	return POSITION_SWIPE_ERROR_DATA_IS_NOT_EXIST;
}

int PositionSwipeMgr::SetVecPositionAxis(string strDscID, int nZoomRatio, int nPosX, int nPosY)
{
	int nRet = CheckValidOfList();
	if (nRet <= 0) return nRet;

	int nValidFourPoints = 0;
	nRet = getCountValidFourPointsInList(nValidFourPoints);
	if (nRet <= 0) return nRet;
	if (nValidFourPoints <= 1) return POSITION_SWIPE_ERROR_VALID_FOURPOINTS_SIZE_IS_LESS_THAN_TWO;

	AscSortByKey();
	nRet = ShiftKeyToBegin(strDscID);
	if (nRet != LIST_MANAGER_OK) return nRet;

	StImageInfo stImageInfo;
	getImageInfoByIndex(0, stImageInfo);
	int nPositionSwipeLength = stImageInfo.nPositionSwipeLength;
	Point2d ptInputPoint = Point2d(nPosX, nPosY + nPositionSwipeLength/**fZoomRatio*/);

	StSwipePoint stSwipePoint;
	stSwipePoint.nZoomRatio = nZoomRatio;
	stSwipePoint.ptCenterPoint = ptInputPoint;
	stSwipePoint.ptViewPoint = Point2d(ptInputPoint.x, ptInputPoint.y - nPositionSwipeLength);

	setSwipePointByIndex(0, stSwipePoint);

	for (int i = 1; i < GetListSize(); i++)
	{
		int nIndex1 = i - 1;
		int nIndex2 = i;

		Mat mHomography;
		if (isFourPointsValid(nIndex2) == POSITION_SWIPE_OK)
		{
			while (isFourPointsValid(nIndex1) != POSITION_SWIPE_OK)
			{
				nIndex1--;
				if (nIndex1 < 0)
					nIndex1 = GetListSize() + nIndex1;
			}

			getSwipePointByIndex(nIndex1, stSwipePoint);
			Point2d ptCenterPoint = stSwipePoint.ptCenterPoint;

			getHomographyAtPairIndex(nIndex1, nIndex2, mHomography);

			if (m_HomographyMgr.CalcPoint(mHomography, ptCenterPoint) != POSITION_SWIPE_OK) continue;

			StImageInfo stImageInfo;
			getImageInfoByIndex(nIndex2, stImageInfo);
			nPositionSwipeLength = stImageInfo.nPositionSwipeLength;

			stSwipePoint.nZoomRatio = nZoomRatio;
			stSwipePoint.ptCenterPoint = ptCenterPoint;
			stSwipePoint.ptViewPoint = Point2d(ptCenterPoint.x, ptCenterPoint.y - nPositionSwipeLength);

			setSwipePointByIndex(nIndex2, stSwipePoint);
		}
	}

	return nRet;
}

int PositionSwipeMgr::SetVecPositionAxis(string strDscID, int nPositionLength, int nZoomRatio, int nPosX, int nPosY)
{
	int nRet = CheckValidOfList();
	if (nRet <= 0) return nRet;

	int nValidFourPoints = 0;
	nRet = getCountValidFourPointsInList(nValidFourPoints);
	if (nRet <= 0) return nRet;
	if (nValidFourPoints <= 1) return POSITION_SWIPE_ERROR_VALID_FOURPOINTS_SIZE_IS_LESS_THAN_TWO;

	AscSortByKey();
	nRet = ShiftKeyToBegin(strDscID);
	if (nRet != LIST_MANAGER_OK) return nRet;

	nRet = recalculatePositionSwipeData(0, nPosX, nPosY, nPositionLength);
	if (nRet != POSITION_SWIPE_OK) return nRet;

	StImageInfo stImageInfo;
	getImageInfoByIndex(0, stImageInfo);
	int nPositionSwipeLength = nPositionLength;
	Point2d ptInputPoint = Point2d(nPosX, nPosY + nPositionSwipeLength/**fZoomRatio*/);

	StSwipePoint stSwipePoint;
	stSwipePoint.nZoomRatio = nZoomRatio;
	stSwipePoint.ptCenterPoint = ptInputPoint;
	stSwipePoint.ptViewPoint = Point2d(ptInputPoint.x, ptInputPoint.y - nPositionSwipeLength);

	setSwipePointByIndex(0, stSwipePoint);

	for (int i = 1; i < GetListSize(); i++)
	{
		int nIndex1 = i - 1;
		int nIndex2 = i;

		Mat mHomography;
		if (isFourPointsValid(nIndex2) == POSITION_SWIPE_OK)
		{
			while (isFourPointsValid(nIndex1) != POSITION_SWIPE_OK)
			{
				nIndex1--;
				if (nIndex1 < 0)
					nIndex1 = GetListSize() + nIndex1;
			}

			getSwipePointByIndex(nIndex1, stSwipePoint);
			Point2d ptCenterPoint = stSwipePoint.ptCenterPoint;

			getHomographyAtPairIndex(nIndex1, nIndex2, mHomography);

			if (m_HomographyMgr.CalcPoint(mHomography, ptCenterPoint) != POSITION_SWIPE_OK) continue;

			StImageInfo stImageInfo;
			getImageInfoByIndex(nIndex2, stImageInfo);
			nPositionSwipeLength = nPositionLength;

			stSwipePoint.nZoomRatio = nZoomRatio;
			stSwipePoint.ptCenterPoint = ptCenterPoint;
			stSwipePoint.ptViewPoint = Point2d(ptCenterPoint.x, ptCenterPoint.y - nPositionSwipeLength);

			setSwipePointByIndex(nIndex2, stSwipePoint);
		}
	}

	return nRet;
}

int PositionSwipeMgr::SetVecPositionAxis(string strDscID, int nPositionLength, int nZoomRatio, int nPosX, int nPosY, vector<Point2d>& vecLog)
{
	int nRet = CheckValidOfList();
	if (nRet <= 0) return nRet;

	int nValidFourPoints = 0;
	nRet = getCountValidFourPointsInList(nValidFourPoints);
	if (nRet <= 0) return nRet;
	if (nValidFourPoints <= 1) return POSITION_SWIPE_ERROR_VALID_FOURPOINTS_SIZE_IS_LESS_THAN_TWO;

	AscSortByKey();
	nRet = ShiftKeyToBegin(strDscID);
	if (nRet != LIST_MANAGER_OK) return nRet;

	nRet = recalculatePositionSwipeData(0, nPosX, nPosY, nPositionLength);
	if (nRet != POSITION_SWIPE_OK) return nRet;

	StImageInfo stImageInfo;
	getImageInfoByIndex(0, stImageInfo);
	int nPositionSwipeLength = nPositionLength;
	Point2d ptInputPoint = Point2d(nPosX, nPosY + nPositionSwipeLength/**fZoomRatio*/);

	StSwipePoint stSwipePoint;
	stSwipePoint.nZoomRatio = nZoomRatio;
	stSwipePoint.ptCenterPoint = ptInputPoint;
	stSwipePoint.ptViewPoint = Point2d(ptInputPoint.x, ptInputPoint.y - nPositionSwipeLength);

	setSwipePointByIndex(0, stSwipePoint);

	for (int i = 1; i < GetListSize(); i++)
	{
		int nIndex1 = i - 1;
		int nIndex2 = i;

		Mat mHomography;
		if (isFourPointsValid(nIndex2) == POSITION_SWIPE_OK)
		{
			while (isFourPointsValid(nIndex1) != POSITION_SWIPE_OK)
			{
				nIndex1--;
				if (nIndex1 < 0)
					nIndex1 = GetListSize() + nIndex1;
			}

			getSwipePointByIndex(nIndex1, stSwipePoint);
			Point2d ptCenterPoint = stSwipePoint.ptCenterPoint;

			getHomographyAtPairIndex(nIndex1, nIndex2, mHomography);

			if (m_HomographyMgr.CalcPoint(mHomography, ptCenterPoint) != POSITION_SWIPE_OK) continue;

			StImageInfo stImageInfo;
			getImageInfoByIndex(nIndex2, stImageInfo);
			nPositionSwipeLength = nPositionLength;

			stSwipePoint.nZoomRatio = nZoomRatio;
			stSwipePoint.ptCenterPoint = ptCenterPoint;
			stSwipePoint.ptViewPoint = Point2d(ptCenterPoint.x, ptCenterPoint.y - nPositionSwipeLength);

			setSwipePointByIndex(nIndex2, stSwipePoint);
			
			vecLog.push_back(stSwipePoint.ptViewPoint);
		}
	}

	return nRet;
}

int PositionSwipeMgr::GetMovedAxis(string strDscID, int& nZoomRatio, int& nPosX, int& nPosY)
{
	for (int i = 0; i < GetListSize(); i++)
	{
		string strCurrentDscID;
		GetKeyByIndex(i, strCurrentDscID);

		if (strCurrentDscID.compare(strDscID) == 0)
		{
			StSwipePoint stSwipePoint;
			getSwipePointByIndex(i, stSwipePoint);

			nZoomRatio = stSwipePoint.nZoomRatio;
			nPosX = static_cast<int>(stSwipePoint.ptViewPoint.x);
			nPosY = static_cast<int>(stSwipePoint.ptViewPoint.y);

			return POSITION_SWIPE_OK;
		}
	}
	return POSITION_SWIPE_ERROR_DATA_IS_NOT_EXIST;
}

int PositionSwipeMgr::GetPositionSwipeLength(string strDscID, int& nPositionLength)
{
	for (int i = 0; i < GetListSize(); i++)
	{
		string strCurrentDscID;
		GetKeyByIndex(i, strCurrentDscID);

		if (strCurrentDscID.compare(strDscID) == 0)
		{
			StSwipePoint stSwipePoint;
			getSwipePointByIndex(i, stSwipePoint);

			nPositionLength =
				static_cast<int>(stSwipePoint.ptCenterPoint.y) -
				static_cast<int>(stSwipePoint.ptViewPoint.y);

			return POSITION_SWIPE_OK;
		}
	}
	return POSITION_SWIPE_ERROR_DATA_IS_NOT_EXIST;
}

int PositionSwipeMgr::convertSquare(StImageInfo stImageInfo, StAdjustData stAdjustData, Mat rotationMat, int& nX, int& nY)
{
	int nRet = isAdjustDataValid(stAdjustData);
	if (nRet != POSITION_SWIPE_OK)
		return nRet;

	double dbMarginScale = 1. /
		((double)(stAdjustData.nMarginWidth) / (double)stImageInfo.nWidth);

	double tempMat_[3] = {
		static_cast<double>(nX),
		static_cast<double>(nY),
		1. };
	Mat tempMat(3, 1, CV_64F, tempMat_);
	Mat resultMat = rotationMat * tempMat;

	nX = static_cast<int>(resultMat.at<double>(0));
	nY = static_cast<int>(resultMat.at<double>(1));

	nX += static_cast<int>(stAdjustData.dMoveX);
	nY += static_cast<int>(stAdjustData.dMoveY);

	int nTempSquareX = nX - stAdjustData.nMarginX;
	int nTempSquarey = nY - stAdjustData.nMarginY;

	nX = static_cast<int>(nTempSquareX * dbMarginScale);
	nY = static_cast<int>(nTempSquarey * dbMarginScale);
	
	return nRet;
}

int PositionSwipeMgr::getWarpMat(StAdjustData stAdjustData, Mat& mWarpMat)
{
	int nRet = isAdjustDataValid(stAdjustData);
	if (nRet != POSITION_SWIPE_OK)
		return nRet;

	double dScale = stAdjustData.dScale;
	double dAngle = stAdjustData.dAngle;
	Point2d ptRotateCenter = Point2d(stAdjustData.dRotateX, stAdjustData.dRotateY);

	if (dScale == 0)
	{
		dScale = 1.;
		dAngle += -90.;
	}

	dAngle = -1 * (dAngle + 90.);
	mWarpMat = getRotationMatrix2D(ptRotateCenter, dAngle, dScale);

	return nRet;
}

int PositionSwipeMgr::isFourPointsValid(int nIndex)
{
	int nRet = CheckValidOfIndex(nIndex);
	if (nRet != LIST_MANAGER_OK) return nRet;

	StFourPoints stFourPoints;
	nRet = getFourPointsByIndex(nIndex, stFourPoints);
	if (nRet != LIST_MANAGER_OK) return nRet;

	if ((stFourPoints.nX1 == -1 && stFourPoints.nY1 == -1) &&
		(stFourPoints.nX2 == -1 && stFourPoints.nY2 == -1) &&
		(stFourPoints.nX3 == -1 && stFourPoints.nY3 == -1) &&
		(stFourPoints.nX4 == -1 && stFourPoints.nY4 == -1))
		return POSITION_SWIPE_ERROR_INVALID_FOURPOINTS;
	else if ((stFourPoints.nX1 == 0 && stFourPoints.nY1 == 0) &&
		(stFourPoints.nX2 == 0 && stFourPoints.nY2 == 0) &&
		(stFourPoints.nX3 == 0 && stFourPoints.nY3 == 0) &&
		(stFourPoints.nX4 == 0 && stFourPoints.nY4 == 0))
		return POSITION_SWIPE_ERROR_INVALID_FOURPOINTS;
	else
		return POSITION_SWIPE_OK;
}

int PositionSwipeMgr::isAdjustDataValid(StAdjustData stAdjustData)
{
	if ((stAdjustData.dMoveX == 0.0 && stAdjustData.dMoveY == 0.0) &&
		(stAdjustData.dRotateX == 0.0 && stAdjustData.dRotateY == 0.0) &&
		(stAdjustData.dAngle == 0.0 && stAdjustData.dScale == 0.0) &&
		(stAdjustData.nMarginX == 0 && stAdjustData.nMarginY == 0) &&
		(stAdjustData.nMarginWidth == 0 && stAdjustData.nMarginHeight == 0))
		return POSITION_SWIPE_ERROR_INVALID_ADJUSTDATA;
	else
		return POSITION_SWIPE_OK;
}

int PositionSwipeMgr::getCountValidFourPointsInList(int& nCount)
{
	int nRet = CheckValidOfList();
	if (nRet != LIST_MANAGER_OK) return nRet;

	nCount = 0;

	for (int i = 0; i < GetListSize(); i++)
		if (isFourPointsValid(i) == POSITION_SWIPE_OK)
			nCount++;

	return nRet;
}

int PositionSwipeMgr::getImageInfoByIndex(int nIndex, StImageInfo& stImageInfo)
{
	StPositionSwipeData stPositionSwipeData;
	int nRet = GetDataByIndex(nIndex, stPositionSwipeData);
	if (nRet != LIST_MANAGER_OK) return nRet;

	stImageInfo = stPositionSwipeData.stImageInfo;

	return nRet;
}

int PositionSwipeMgr::getImageInfoByChannel(int nChannel, StImageInfo& stImageInfo)
{
	StPositionSwipeData stPositionSwipeData;
	int nRet = GetDataByKey(nChannel, stPositionSwipeData);
	if (nRet != LIST_MANAGER_OK) return nRet;

	stImageInfo = stPositionSwipeData.stImageInfo;

	return nRet;
}

int PositionSwipeMgr::setFourPointsByIndex(int nIndex, const StFourPoints& stFourPoints)
{
	StPositionSwipeData stPositionSwipeData;
	int nRet = GetDataByIndex(nIndex, stPositionSwipeData);
	if (nRet != LIST_MANAGER_OK) return nRet;

	stPositionSwipeData.stFourPoints = stFourPoints;

	int nChannel = 0;
	nRet = GetKeyByIndex(nIndex, nChannel);
	if (nRet != LIST_MANAGER_OK) return nRet;

	nRet = SetData(nChannel, stPositionSwipeData);
	if (nRet != LIST_MANAGER_OK) return nRet;

	return nRet;
}

int PositionSwipeMgr::getFourPointsByIndex(int nIndex, StFourPoints& stFourPoints)
{
	StPositionSwipeData stPositionSwipeData;
	int nRet = GetDataByIndex(nIndex, stPositionSwipeData);
	if (nRet != LIST_MANAGER_OK) return nRet;

	stFourPoints = stPositionSwipeData.stFourPoints;

	return nRet;
}

int PositionSwipeMgr::setSwipePointByIndex(int nIndex, const StSwipePoint& stSwipePoint)
{
	StPositionSwipeData stPositionSwipeData;
	int nRet = GetDataByIndex(nIndex, stPositionSwipeData);
	if (nRet != LIST_MANAGER_OK) return nRet;

	stPositionSwipeData.stSwipePoint = stSwipePoint;

	int nChannel = 0;
	nRet = GetKeyByIndex(nIndex, nChannel);
	if (nRet != LIST_MANAGER_OK) return nRet;

	nRet = SetData(nChannel, stPositionSwipeData);
	if (nRet != LIST_MANAGER_OK) return nRet;

	return nRet;
}

int PositionSwipeMgr::getSwipePointByIndex(int nIndex, StSwipePoint& stSwipePoint)
{
	StPositionSwipeData stPositionSwipeData;
	int nRet = GetDataByIndex(nIndex, stPositionSwipeData);
	if (nRet != LIST_MANAGER_OK) return nRet;

	stSwipePoint = stPositionSwipeData.stSwipePoint;

	return nRet;
}

int PositionSwipeMgr::getCenterPointByIndex(int nIndex, Point2d& ptCenterPoint)
{
	int nRet = CheckValidOfIndex(nIndex);

	StPositionSwipeData stPositionSwipeData;
	nRet = GetDataByIndex(nIndex, stPositionSwipeData);
	if (nRet != LIST_MANAGER_OK) return nRet;

	ptCenterPoint = stPositionSwipeData.stSwipePoint.ptCenterPoint;

	return nRet;
}

int PositionSwipeMgr::addHomographyAtPairIndex(int nIndex1, int nIndex2, const Mat& mHomography)
{
	int nRet = 0;
	int nChannel1 = 0, nChannel2 = 0;

	nRet = GetKeyByIndex(nIndex1, nChannel1);
	if (nRet != LIST_MANAGER_OK) return nRet;

	nRet = GetKeyByIndex(nIndex2, nChannel2);
	if (nRet != LIST_MANAGER_OK) return nRet;

	nRet = m_HomographyMgr.AddHomographyAtPairChannel(nChannel1, nChannel2, mHomography);

	return nRet;
}

int PositionSwipeMgr::getHomographyAtPairIndex(int nIndex1, int nIndex2, Mat& mHomography)
{
	int nRet = 0;
	int nChannel1 = 0, nChannel2 = 0;

	nRet = GetKeyByIndex(nIndex1, nChannel1);
	if (nRet != LIST_MANAGER_OK) return nRet;

	nRet = GetKeyByIndex(nIndex2, nChannel2);
	if (nRet != LIST_MANAGER_OK) return nRet;

	nRet = m_HomographyMgr.GetHomographyAtPairChannel(nChannel1, nChannel2, mHomography);	

	return nRet;
}

void PositionSwipeMgr::GeneratePositionSwipePayload()
{
	m_nPositionSwipeDataSize = 0;

	if (m_szPositionSwipeData != nullptr)
		free(m_szPositionSwipeData);

	unsigned char* szPositionSwipeData = nullptr;
	szPositionSwipeData = (unsigned char*)malloc(sizeof(short int) * (4 + 9 * GetListSize()));

	if (GetListSize() <= 0)
		return;

	StImageInfo stImageInfo;
	getImageInfoByIndex(GetListSize() - 1, stImageInfo);

	short int nWidth = static_cast<short int>(stImageInfo.nWidth);
	short int nHeight = static_cast<short int>(stImageInfo.nHeight);
	short int nPositionLength = static_cast<short int>(stImageInfo.nPositionSwipeLength);
	short int nCount = static_cast<short int>(GetListSize());

	nWidth = htons(nWidth);
	::memmove(&szPositionSwipeData[m_nPositionSwipeDataSize], &nWidth, sizeof(nWidth));
	m_nPositionSwipeDataSize += 2;

	nHeight = htons(nHeight);
	::memmove(&szPositionSwipeData[m_nPositionSwipeDataSize], &nHeight, sizeof(nHeight));
	m_nPositionSwipeDataSize += 2;

	nPositionLength = htons(nPositionLength);
	::memmove(&szPositionSwipeData[m_nPositionSwipeDataSize], &nPositionLength, sizeof(nPositionLength));
	m_nPositionSwipeDataSize += 2;

	nCount = htons(nCount);
	::memmove(&szPositionSwipeData[m_nPositionSwipeDataSize], &nCount, sizeof(nCount));
	m_nPositionSwipeDataSize += 2;

	for (int i = 0; i < GetListSize(); i++)
	{
		int nRet = isFourPointsValid(i);
		if (nRet == POSITION_SWIPE_ERROR_INVALID_FOURPOINTS) continue;

		StFourPoints stFourPoints;
		getFourPointsByIndex(i, stFourPoints);
		
		int nChannel = 0;
		GetKeyByIndex(i, nChannel);

		short int nValue = static_cast<short int>(nChannel);
		nValue = htons(nValue);
		::memmove(&szPositionSwipeData[m_nPositionSwipeDataSize], &nValue, sizeof(nValue));
		m_nPositionSwipeDataSize += 2;

		nValue = static_cast<short int>(stFourPoints.nX1);
		nValue = htons(nValue);
		::memmove(&szPositionSwipeData[m_nPositionSwipeDataSize], &nValue, sizeof(nValue));
		m_nPositionSwipeDataSize += 2;

		nValue = static_cast<short int>(stFourPoints.nY1);
		nValue = htons(nValue);
		::memmove(&szPositionSwipeData[m_nPositionSwipeDataSize], &nValue, sizeof(nValue));
		m_nPositionSwipeDataSize += 2;

		nValue = static_cast<short int>(stFourPoints.nX2);
		nValue = htons(nValue);
		::memmove(&szPositionSwipeData[m_nPositionSwipeDataSize], &nValue, sizeof(nValue));
		m_nPositionSwipeDataSize += 2;

		nValue = static_cast<short int>(stFourPoints.nY2);
		nValue = htons(nValue);
		::memmove(&szPositionSwipeData[m_nPositionSwipeDataSize], &nValue, sizeof(nValue));
		m_nPositionSwipeDataSize += 2;

		nValue = static_cast<short int>(stFourPoints.nX3);
		nValue = htons(nValue);
		::memmove(&szPositionSwipeData[m_nPositionSwipeDataSize], &nValue, sizeof(nValue));
		m_nPositionSwipeDataSize += 2;

		nValue = static_cast<short int>(stFourPoints.nY3);
		nValue = htons(nValue);
		::memmove(&szPositionSwipeData[m_nPositionSwipeDataSize], &nValue, sizeof(nValue));
		m_nPositionSwipeDataSize += 2;

		nValue = static_cast<short int>(stFourPoints.nX4);
		nValue = htons(nValue);
		::memmove(&szPositionSwipeData[m_nPositionSwipeDataSize], &nValue, sizeof(nValue));
		m_nPositionSwipeDataSize += 2;

		nValue = static_cast<short int>(stFourPoints.nY4);
		nValue = htons(nValue);
		::memmove(&szPositionSwipeData[m_nPositionSwipeDataSize], &nValue, sizeof(nValue));
		m_nPositionSwipeDataSize += 2;
	}

	int nByteSize = m_nPositionSwipeDataSize;
	unsigned char* szTemp = rbsp2ebsp(&m_nPositionSwipeDataSize, szPositionSwipeData, nByteSize);

	free(szPositionSwipeData);

	m_szPositionSwipeData = szTemp;
}

int PositionSwipeMgr::GenerateMultiviewPayloadByChannel(int nChannel)
{
	m_nMultiviewDataSizeByChannel = 0;

	if (m_szMultiviewDataByChannel != nullptr)
		free(m_szMultiviewDataByChannel);

	const int nMultiviewPayloadSize = 26;

	unsigned char* szMultiviewDataByChannel = nullptr;
	szMultiviewDataByChannel = (unsigned char*)malloc(
		nMultiviewPayloadSize);

	StImageInfo stImageInfo;
	int nRet = getImageInfoByChannel(nChannel, stImageInfo);
	if (nRet != LIST_MANAGER_OK)
		return nRet;

	short int nWidth = static_cast<short int>(stImageInfo.nWidth);
	short int nHeight = static_cast<short int>(stImageInfo.nHeight);

	nWidth = htons(nWidth);
	::memmove(&szMultiviewDataByChannel[m_nMultiviewDataSizeByChannel], &nWidth, sizeof(nWidth));
	m_nMultiviewDataSizeByChannel += 2;

	nHeight = htons(nHeight);
	::memmove(&szMultiviewDataByChannel[m_nMultiviewDataSizeByChannel], &nHeight, sizeof(nHeight));
	m_nMultiviewDataSizeByChannel += 2;

	int nZoomRatio = 0;
	int nPosX = 0;
	int nPosY = 0;
	nRet = GetMovedAxis(nChannel, nZoomRatio, nPosX, nPosY);
	if (nRet != POSITION_SWIPE_OK)
		return nRet;

	short int nValue = static_cast<short int>(nZoomRatio);
	nValue = htons(nValue);
	::memmove(&szMultiviewDataByChannel[m_nMultiviewDataSizeByChannel], &nValue, sizeof(nValue));
	m_nMultiviewDataSizeByChannel += 2;

	nValue = static_cast<short int>(nPosX);
	nValue = htons(nValue);
	::memmove(&szMultiviewDataByChannel[m_nMultiviewDataSizeByChannel], &nValue, sizeof(nValue));
	m_nMultiviewDataSizeByChannel += 2;

	nValue = static_cast<short int>(nPosY);
	nValue = htons(nValue);
	::memmove(&szMultiviewDataByChannel[m_nMultiviewDataSizeByChannel], &nValue, sizeof(nValue));
	m_nMultiviewDataSizeByChannel += 2;

	int nMarginPadding = 34952;

	for (int i = 0; i < 8; i++)
	{
		//Margin Padding
		nValue = static_cast<short int>(nMarginPadding);
		nValue = htons(nValue);
		::memmove(&szMultiviewDataByChannel[m_nMultiviewDataSizeByChannel], &nValue, sizeof(nValue));
		m_nMultiviewDataSizeByChannel += 2;
	}

	int nByteSize = m_nMultiviewDataSizeByChannel;
	unsigned char* szTemp = rbsp2ebsp(&m_nMultiviewDataSizeByChannel, szMultiviewDataByChannel, nByteSize);

	free(szMultiviewDataByChannel);

	m_szMultiviewDataByChannel = szTemp;

	return nRet;
}

int PositionSwipeMgr::GetPositionSwipeDataForSEI(unsigned char*& szPositionSwipeData, int& nLength)
{
	GeneratePositionSwipePayload();

	szPositionSwipeData = m_szPositionSwipeData;
	nLength = m_nPositionSwipeDataSize;

	return 1;
}

int PositionSwipeMgr::GetSEIMultiviewDataByChannel(int nChannel, unsigned char*& szMultiviewData, int& nLength)
{
	int nRet = GenerateMultiviewPayloadByChannel(nChannel);
	if (nRet != POSITION_SWIPE_OK)
		return nRet;

	szMultiviewData = m_szMultiviewDataByChannel;
	nLength = m_nMultiviewDataSizeByChannel;

	return nRet;
}

unsigned char* PositionSwipeMgr::rbsp2ebsp(int* ebsp_size, unsigned char* rbsp, int rbsp_size)
{
	unsigned char* ebsp = (unsigned char*)malloc(sizeof(unsigned char) * rbsp_size);
	int j = 0;
	int count = 0;

	for (int i = 0; i < rbsp_size; i++) {
		if (count >= 2 && !(rbsp[i] & 0xFC)) {
			j++;
			//realloc(ebsp, rbsp_size + j - i);
			ebsp = (unsigned char*)realloc(ebsp, rbsp_size + j - i);
			ebsp[j - 1] = 0x03;

			count = 0;
		}
		ebsp[j] = rbsp[i];

		if (rbsp[i] == 0x00)
			count++;
		else
			count = 0;
		j++;
	}
	*ebsp_size = j;

	return ebsp;
}

string PositionSwipeMgr::GetVersion()
{
	return POSITION_TRACKING_MODULE_VERSION;
}