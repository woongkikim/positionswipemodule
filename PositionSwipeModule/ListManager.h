#pragma once

#include <mutex>

#include <vector>
#include <string>
using namespace std;

namespace CPP
{
	template <class TData>
	class ListManager
	{
	public:
		ListManager(bool bUseMutex = true);
		~ListManager();

	public:
		int AddData(int nKey, string strKey, const TData& tData);
		int SetData(int nKey, string strKey, const TData& tData);
		int SetData(int nKey, const TData& tData);

		int IsExistData(int nKey);
		int IsExistData(string strKey);

		int GetKeyByIndex(int nIndex, int& nKey);
		int GetKeyByIndex(int nIndex, string& strKey);

		int GetDataByIndex(int nIndex, TData& tData);

		int GetDataByKey(int nKey, TData& tData);
		int GetDataByKey(string strKey, TData& tData);

		int GetListSize();
		int CheckValidOfList();
		int CheckValidOfIndex(int nIndex);

		int AscSortByKey();
		int DesSortByKey();
		int ShiftKeyToBegin(int nKey);
		int ShiftKeyToBegin(string strKey);

		int ClearList();

		//int GetKeyAs(const TData& tData, int& nKey);
		//int GetKeyAs(const TData& tData, string& strKey);

	protected:
		enum LIST_MANAGER
		{
			LIST_MANAGER_OK = 0x01,

			LIST_MANAGER_ERROR_DATA_IS_NOT_EXIST = -0x01,
			LIST_MANAGER_ERROR_DATA_ALREADY_EXIST = -0x02,
			LIST_MANAGER_ERROR_LIST_IS_EMPTY = -0x03,
			LIST_MANAGER_ERROR_LIST_IS_NOT_VALID = -0x04,
			
			LIST_MANAGER_ERROR_INDEX_IS_NEGATIVE = -0x05,

			LIST_MANAGER_ERROR_MUTEX_IS_DISABLED = -0x06,
			LIST_MANAGER_ERROR_NOT_NEED_TO_ROTATE = -0x07,
		};

	private:
		int getIndexBy(int nKey, int& nIndex);
		int getIndexBy(string strKey, int& nIndex);

		int shiftIndexToBegin(int nIndex);

	private:
		struct StElement
		{
			StElement()
			{
				nKey = 0;
			}
			int nKey;
			string strKey;
			TData tData;
		};
		vector<StElement> m_vecStElement;

	private:
		bool m_bUseMutex;
		mutex m_mDataManagerMutex;

		int mutexLock();
		int mutexUnlock();
	};
};

#include "ListManagerImplementation.hpp"
