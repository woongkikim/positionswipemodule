#include "PositionSwipeMgrCSharp.h"

PositionSwipeMgr* CreatePositionSwipeMgrCSharp()
{
	return new PositionSwipeMgr;
}

int DeletePositionSwipeMgrCSharp(PositionSwipeMgr* positionSwipeMgr)
{
	delete positionSwipeMgr;
	positionSwipeMgr = nullptr;

	return 1;
}

int AddPositionSwipeDataCSharp(
	PositionSwipeMgr* positionSwipeMgr,

	int nChannel, int* strDscID,
	int nWidth, int nHeight, int nPositionSwipeLength,

	double dMoveX, double dMoveY,
	double dRotateX, double dRotateY,
	double dAngle, double dScale,
	int nMarginX,
	int nMarginY,
	int nMarginWidth,
	int nMarginHeight,

	int nX1, int nY1,
	int nX2, int nY2,
	int nX3, int nY3,
	int nX4, int nY4)
{
	string strDsc((char*)strDscID);
	return positionSwipeMgr->AddPositionSwipeData(
		nChannel, strDsc, nWidth, nHeight, nPositionSwipeLength, 
		dMoveX, dMoveY,	dRotateX, dRotateY,	dAngle, dScale,
		nMarginX, nMarginY, nMarginWidth, nMarginHeight,
		nX1, nY1, nX2, nY2, nX3, nY3, nX4, nY4);
}

int ApplyPositionSwipeDataCSharp(PositionSwipeMgr* positionSwipeMgr)
{
	return positionSwipeMgr->ApplyPositionSwipeData();
}

int SetVecPositionAxisCSharp(
	PositionSwipeMgr* positionSwipeMgr,
	int nChannel, int nZoomRatio, int nPosX, int nPosY)
{
	return positionSwipeMgr->SetVecPositionAxis(nChannel, nZoomRatio, nPosX, nPosY);
}

int GetMovedAxisCSharp(
	PositionSwipeMgr* positionSwipeMgr,
	int nChannel, int& nZoomRatio, int& nPosX, int& nPosY)
{
	return positionSwipeMgr->GetMovedAxis(nChannel, nZoomRatio, nPosX, nPosY);
}

int GetPositionSwipeDataForSEICSharp(PositionSwipeMgr* positionSwipeMgr,
	unsigned char*& szPositionSwipeData,
	int& nLength)
{
	return positionSwipeMgr->GetPositionSwipeDataForSEI(szPositionSwipeData, nLength);
}

int GetSEIMultiviewDataByChannelCSharp(PositionSwipeMgr* positionSwipeMgr,
	int nChannel,
	unsigned char*& szPositionSwipeData,
	int& nLength)
{
	return positionSwipeMgr->GetSEIMultiviewDataByChannel(nChannel, szPositionSwipeData, nLength);
}

