#pragma once

#include "PositionSwipeMgr.h"

#ifdef CREATEDLL_EXPORTS
#define POSITION_SWIPE_MODULE_CSHARP __declspce(dllexport)
#else
#define POSITION_SWIPE_MODULE_CSHARP __declspec(dllexport)
#endif

extern "C" 
{
	__declspec(dllexport) 
		PositionSwipeMgr* CreatePositionSwipeMgrCSharp();
	__declspec(dllexport) 
		int DeletePositionSwipeMgrCSharp(
		PositionSwipeMgr* positionSwipeMgr);
	
	__declspec(dllexport)
	int AddPositionSwipeDataCSharp(
		PositionSwipeMgr* positionSwipeMgr,

		int nChannel, int* strDscID,
		int nWidth, int nHeight, int nPositionSwipeLength,

		double dMoveX, double dMoveY,
		double dRotateX, double dRotateY,
		double dAngle, double dScale,
		int nMarginX,
		int nMarginY,
		int nMarginWidth,
		int nMarginHeight,

		int nX1, int nY1,
		int nX2, int nY2,
		int nX3, int nY3,
		int nX4, int nY4);

	__declspec(dllexport)
	int ApplyPositionSwipeDataCSharp(
		PositionSwipeMgr* positionSwipeMgr);

	__declspec(dllexport)
	int SetVecPositionAxisCSharp(
		PositionSwipeMgr* positionSwipeMgr,
		int nChannel, int nZoomRatio, int nPosX, int nPosY);
	__declspec(dllexport)
	int GetMovedAxisCSharp(
		PositionSwipeMgr* positionSwipeMgr,
		int nChannel, int& nZoomRatio, int& nPosX, int& nPosY);
	__declspec(dllexport)
	int GetPositionSwipeLengthCSharp(
		PositionSwipeMgr* positionSwipeMgr,
		int& nPositionSwipeLength);

	__declspec(dllexport)
	int GetPositionSwipeDataForSEICSharp(
		PositionSwipeMgr* positionSwipeMgr,
		unsigned char*& szPositionSwipeData,
		int& nLength);

	__declspec(dllexport)
	int GetSEIMultiviewDataByChannelCSharp(
		PositionSwipeMgr* positionSwipeMgr,
		int nChannel,
		unsigned char*& szPositionSwipeData,
		int& nLength);
}
