#pragma once

#include <map>
#include <winsock.h>
#pragma comment(lib,"ws2_32.lib")

#include "ListManager.h"
#include "PositionSwipeDataStruct.h"
#include "HomographyMgr.h"

class PositionSwipeMgr : 
	public CPP::ListManager<StPositionSwipeData>
{
private:
	const string POSITION_TRACKING_MODULE_VERSION = 
		"POSITION_TRACKING_MODULE_VERSION v1.3";

public:
	string GetVersion();

public:
	PositionSwipeMgr();
	~PositionSwipeMgr();

public:
	int AddPositionSwipeData(
		int nChannel, string strDscID,

		int nWidth, int nHeight, int nPositionSwipeLength,

		double dMoveX, double dMoveY,
		double dRotateX, double dRotateY,
		double dAngle, double dScale,
		int nMarginX,
		int nMarginY,
		int nMarginWidth,
		int nMarginHeight,

		int nX1, int nY1,
		int nX2, int nY2,
		int nX3, int nY3,
		int nX4, int nY4);

	int ApplyPositionSwipeData();

	int SetVecPositionAxis(int nChannel, int nZoomRatio, int nPosX, int nPosY);
	int GetMovedAxis(int nChannel, int& nZoomRatio, int& nPosX, int& nPosY);

	int SetVecPositionAxis(string strDscID, int nZoomRatio, int nPosX, int nPosY);
	int SetVecPositionAxis(string strDscID, int nPositionLength, int nZoomRatio, int nPosX, int nPosY);
	int SetVecPositionAxis(string strDscID, int nPositionLength, int nZoomRatio, int nPosX, int nPosY, vector<Point2d>& vecLog);
	int GetMovedAxis(string strDscID, int& nZoomRatio, int& nPosX, int& nPosY);

	int GetPositionSwipeLength(string strDscID, int &nPositionLength);

	int GetSEIMultiviewDataByChannel(int nChannel, unsigned char*& szMultiviewData, int& nLength);

private:
	enum POSITION_SWIPE
	{
		POSITION_SWIPE_OK = 0x01,

		POSITION_SWIPE_ERROR_DATA_IS_NOT_EXIST = -0x02,
		POSITION_SWIPE_ERROR_DATA_ALREADY_EXIST = -0x03,
		POSITION_SWIPE_ERROR_LIST_IS_EMPTY = -0x04,		

		POSITION_SWIPE_ERROR_INVALID_FOURPOINTS = -0x06,
		POSITION_SWIPE_ERROR_INVALID_ADJUSTDATA = -0x07,
		POSITION_SWIPE_ERROR_HOMOGRAPHY_IS_NOT_EXIST = -0x08,
		POSITION_SWIPE_ERROR_VALID_FOURPOINTS_SIZE_IS_LESS_THAN_TWO = -0x09,
	};

private:
	int convertSquare(StImageInfo stImageInfo, StAdjustData stAdjustData, Mat rotationMat, int& nX, int& nY);
	int getWarpMat(StAdjustData stAdjustData, Mat& mWarpMat);

	int convertFourPointsUsingAdjustData();
	int generateHomographies();

	int recalculatePositionSwipeData(int nIndex, int& nPosX, int& nPosY, int& nPositionLength);

private:
	int isFourPointsValid(int nIndex);
	int isAdjustDataValid(StAdjustData stAdjustData);
	int getCountValidFourPointsInList(int& nCount);

	int getImageInfoByIndex(int nIndex, StImageInfo& stImageInfo);
	int getImageInfoByChannel(int nChannel, StImageInfo& stImageInfo);

	int setFourPointsByIndex(int nIndex, const StFourPoints& stFourPoints);
	int getFourPointsByIndex(int nIndex, StFourPoints& stFourPoints);

	int setSwipePointByIndex(int nIndex, const StSwipePoint& stSwipePoint);
	int getSwipePointByIndex(int nIndex, StSwipePoint& stSwipePoint);

	int getCenterPointByIndex(int nIndex, Point2d& ptCenterPoint);	

private:
	HomographyMgr m_HomographyMgr;

	int addHomographyAtPairIndex(int nIndex1, int nIndex2, const Mat& mHomography);
	int getHomographyAtPairIndex(int nIndex1, int nIndex2, Mat& mHomography);

private:
	unsigned char* m_szPositionSwipeData;
	int m_nPositionSwipeDataSize;

	unsigned char* m_szMultiviewData;
	int m_nMultiviewDataSize;

	unsigned char* m_szMultiviewDataByChannel;
	int m_nMultiviewDataSizeByChannel;

private:
	void GeneratePositionSwipePayload();
	int GenerateMultiviewPayloadByChannel(int nChannel);

public:
	int GetPositionSwipeDataForSEI(unsigned char*& szPositionSwipeData, int& nLength);// m_szPositionSwipeData;

	unsigned char* rbsp2ebsp(int* ebsp_size, unsigned char* rbsp, int rbsp_size);

};


