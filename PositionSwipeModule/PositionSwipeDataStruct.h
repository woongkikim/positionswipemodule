#pragma once

#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/calib3d.hpp"
using namespace cv;

struct StImageInfo
{
	StImageInfo()
	{
		nWidth = 0;
		nHeight = 0;
		nPositionSwipeLength = 0;
	}
	int nWidth;
	int nHeight;
	int nPositionSwipeLength;
};

struct StAdjustData
{
	StAdjustData()
	{
		dMoveX = 0.;
		dMoveY = 0.;
		dRotateX = 0.;
		dRotateY = 0.;
		dAngle = 0.;
		dScale = 0.;

		nMarginX = 0;
		nMarginY = 0;
		nMarginWidth = 0;
		nMarginHeight = 0;
	}
	double dMoveX;
	double dMoveY;
	double dRotateX;
	double dRotateY;
	double dAngle;
	double dScale;

	int nMarginX;
	int nMarginY;
	int nMarginWidth;
	int nMarginHeight;
};

struct StFourPoints
{
	StFourPoints()
	{
		nX1 = 0;
		nY1 = 0;
		nX2 = 0;
		nY2 = 0;
		nX3 = 0;
		nY3 = 0;
		nX4 = 0;
		nY4 = 0;
	}
	int nX1;
	int nY1;
	int nX2;
	int nY2;
	int nX3;
	int nY3;
	int nX4;
	int nY4;
};

struct StSwipePoint
{
	StSwipePoint()
	{
		nZoomRatio = 0;
	}

	int nZoomRatio;
	Point2d ptCenterPoint;
	Point2d ptViewPoint;
};

struct StPositionSwipeData
{
	StPositionSwipeData(){}

	StImageInfo stImageInfo;
	StAdjustData stAdjustData;
	StFourPoints stFourPoints;
	StSwipePoint stSwipePoint;
};
