
#include "PositionSwipeMgr.h"
#include "PositionSwipeMgrCSharp.h"

int main(void)
{
	//PositionSwipeModuleCSharp 클래스 사용 예시
	PositionSwipeMgr positionSwipeMgr;

	srand((unsigned int)time(NULL));
	while (1)
	{
		//-1, -1, -1, -1, -1, -1, -1, -1);//
		int nRet = positionSwipeMgr.AddPositionSwipeData(
			0, "100001", 3840, 2160, 0,
			-2.8, -8.5, 1917., 1356., -89.928, 1., 192, 108, 3468, 1951,
			-1, -1, -1, -1, -1, -1, -1, -1);//866, 1151, 2654, 1101, 3640, 1705, 334, 1865);
		positionSwipeMgr.AddPositionSwipeData(
			1, "100002", 3840, 2160, 0,
			-2.8, 9.5, 1917., 1338., -89.974, 1.043, 192, 108, 3468, 1951,
			668, 1198, 2387, 1076, 3609, 1537, 785, 1914);
		positionSwipeMgr.AddPositionSwipeData(
			2, "100003", 3840, 2160, 0, 
			4.19, 32.5, 1910., 1315., -90.812, 1.116, 192, 108, 3468, 1951,
			532, 1233, 2120, 1074, 3462, 1415, 1354, 1854);
		positionSwipeMgr.AddPositionSwipeData(
			3, "100004", 3840, 2160, 0, 
			-2.2, 26.5, 1912., 1321., -90.541, 1.165, 192, 108, 3468, 1951,
			-1, -1, -1, -1, -1, -1, -1, -1);//475, 1295, 1943, 1087, 3345, 1353, 1763, 1841);
		positionSwipeMgr.AddPositionSwipeData(
			4, "100005", 3840, 2160, 0, 
			-3.8, 29.5, 1918., 1318., -88.302, 1.171, 192, 108, 3468, 1951,
			408, 1406, 1765, 1088, 3250, 1247, 2177, 1834);
		positionSwipeMgr.AddPositionSwipeData(
			5, "100006", 3840, 2160, 0, 
			9.2, 14.5, 1905., 1333., -90.366, 1.013, 192, 108, 3468, 1951,
			98, 1483, 1509, 1044, 3221, 1231, 2741, 2015);
		positionSwipeMgr.AddPositionSwipeData(
			6, "100007", 3840, 2160, 0, 
			-12.8, -7.5, 1927., 1355., -90.042, 0.988, 192, 108, 3468, 1951,
			49, 1645, 1270, 1093, 3067, 1187, 3288, 1914);
		positionSwipeMgr.AddPositionSwipeData(
			7, "100008", 3840, 2160, 0, 
			11.2, 9.5, 1903., 1338., -89.313, 0.942, 192, 108, 3468, 1951,
			83, 1861, 957, 1106, 2814, 1086, 3671, 1807);
		positionSwipeMgr.AddPositionSwipeData(
			8, "100009", 3840, 2160, 0, 
			0.2, -38.5, 1914., 1386., -89.821, 0.961, 192, 108, 3468, 1951,
			542, 2073, 679, 1221, 2504, 1104, 3809, 1664);
		positionSwipeMgr.AddPositionSwipeData(
			9, "100010", 3840, 2160, 0, 
			-4.8, -67.5, 1919., 1415., -89.005, 1., 192, 108, 3468, 1951,
			-1, -1, -1, -1, -1, -1, -1, -1);//1092, 2155, 517, 1340, 2264, 1127, 3731, 1532);
		positionSwipeMgr.AddPositionSwipeData(
			6, "100007", 3840, 2160, 0,
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0, 0, 0, 0,
			49, 1645, 1270, 1093, 3067, 1187, 3288, 1914);//1092, 2155, 517, 1340, 2264, 1127, 3731, 1532);
		
		positionSwipeMgr.ApplyPositionSwipeData();

		//AddPositionSwipeData()로 Data들의 입력이 완료된 후 호출
		//BirdView의 채널이 1이라고 가정하고 ROI의 Zoom 배율 및 중심좌표를 입력 후
		//호출하면 나머지 채널(2, 3, 4, 5)의 ROI 중심좌표를 계산
		int nChannel = 1;
		string strDscID = "100002";
		int nPositionLength = 150;
		int nZoomRatio = 150;
		int nPosX = 1920;
		int nPosY = 1080;
		nRet = positionSwipeMgr.SetVecPositionAxis(strDscID, nPositionLength, nZoomRatio, nPosX, nPosY);

		//SetVecPositionAxis()로 입력된 채널의 좌표정보를 기준으로 
		//계산된 ROI의 중심좌표를 리턴받는 함수
		//nChannel에 해당하는 Zoom배율, 중심좌표값을 획득
		//UFC에 기획된 Multiview 시나리오에서는 해당 메소드가 필요없을수도 있음
		nChannel = 2;
		strDscID = "100003";
		nZoomRatio = 0;
		nPosX = 0;
		nPosY = 0;
		nRet = positionSwipeMgr.GetMovedAxis(strDscID, nZoomRatio, nPosX, nPosY);


		//nChannel = rand() % 10;
		//nX = rand() % 1920;
		//nY = rand() % 1080;

		//double dZoom = (rand() % 1000 + 1000) / 1000.;

		////positionSwipeMgr.SetVecPositionAxis(nChannel, dZoom, nX, nY);
		//positionSwipeMgr.SetVecPositionAxis(8, 160, 1920, 1080);
	}
	return 1;
}
